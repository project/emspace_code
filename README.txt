This code supports, and is required by, the Em Space Installation Profile. It
is a work in progress. It provides support beyond the installation such as help
with theming.

Please content simon at emspace.com.au if there are questions or issues. 